/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chromelauncher;
//JOptionPane.showMessageDialog(null, "。", "欢迎", JOptionPane.INFORMATION_MESSAGE);
/**
 *
 * @author 雅詩
 */
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class MainWindow extends javax.swing.JFrame {

    /**
     * Creates new form MainWindow
     */
    private String chromium_exe;
    private JPanel panel;
    private ResourceBundle transbundle;

    private String get_trans(String key) {
        String val = this.transbundle.getString(key);
        String trans;
        try {
            trans = new String(val.getBytes("ISO-8859-1"), "UTF-8");
        } catch (java.io.UnsupportedEncodingException e) {
            System.out.printf("failed to translate %s %n", key);
            trans = "?";
        }
        return trans;
    }

    public MainWindow(java.util.ResourceBundle transbundle) {
        this.transbundle = transbundle;
        initComponents();
        initWindowIcon();
        createMenuBar();
        chromium_exe = getExecutablePath("chromium");
        find_profiles();
    }

    private void initWindowIcon() {
        ImageIcon webIcon = new ImageIcon("/usr/share/pixmaps/chromium.png");
        setIconImage(webIcon.getImage());
    }

     private void createMenuBar() {

        JMenuBar menubar = new JMenuBar();

        JMenu file = new JMenu("File");
        file.setMnemonic(KeyEvent.VK_F);

        JMenuItem eMenuItem = new JMenuItem("Exit", null);
        eMenuItem.setMnemonic(KeyEvent.VK_E);
        eMenuItem.setToolTipText("Exit application");
        eMenuItem.addActionListener((ActionEvent event) -> {
            System.exit(0);
        });

        file.add(eMenuItem);

        menubar.add(file);

        JMenu edit = new JMenu("Edit");
        file.setMnemonic(KeyEvent.VK_E);

        JMenuItem set_path = new JMenuItem("Path", null);
        set_path.setMnemonic(KeyEvent.VK_P);
        set_path.setToolTipText("Path to Chromium executable");
        set_path.addActionListener(new FindChromiumAction());

        edit.add(set_path);

        menubar.add(edit);

        setJMenuBar(menubar);
    }

    private String getExecutablePath(String filename) {
        Runtime rt = Runtime.getRuntime();
        try {
            Process proc = rt.exec("which " + filename);
            proc.waitFor();
            java.io.BufferedReader input =
                    new java.io.BufferedReader
                            (new java.io.InputStreamReader(proc.getInputStream()));
            String result = input.readLine();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private String[] find_profiles() {
        Map<String, String> env = System.getenv();
        String home = env.get("HOME");
        File chromium_dir = new File(home + File.separator + ".config" + File.separator + "chromium");
        return chromium_dir.list(new ChromiumProfileFilter());
    }

    private AboutWindow startWindow = new AboutWindow();;

    private void initComponents() {

        JList list = new JList(find_profiles());
        list.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    String profile = (String) list.getSelectedValue();
                    System.out.printf("Selected profile: %s%n", profile);
                }
            }
        });

        label_sec = new javax.swing.JLabel();
        file_access_file = new javax.swing.JCheckBox();
        disable_security = new javax.swing.JCheckBox();
        nosandbox = new javax.swing.JCheckBox();
        proc_settings = new javax.swing.JCheckBox();
        label_content = new javax.swing.JLabel();
        disable_js = new javax.swing.JCheckBox();
        disable_java = new javax.swing.JCheckBox();
        disable_plugins = new javax.swing.JCheckBox();
        disable_images = new javax.swing.JCheckBox();
        disk_cache = new javax.swing.JCheckBox();
        hczdz1 = new javax.swing.JTextField();
        dmthczdz0 = new javax.swing.JCheckBox();
        dmthczdz1 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jrysms = new javax.swing.JCheckBox();
        qyzhqhcd = new javax.swing.JCheckBox();
        jLabel8 = new javax.swing.JLabel();
        qzqysqtb = new javax.swing.JCheckBox();
        zgjlxssqan = new javax.swing.JCheckBox();
        yxwytcck = new javax.swing.JCheckBox();
        szyy0 = new javax.swing.JCheckBox();
        szyy1 = new javax.swing.JComboBox<>();
        pac0 = new javax.swing.JCheckBox();
        pac1 = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        autorun = new javax.swing.JCheckBox();
        startBtn = new javax.swing.JButton();
        cmdBtn = new javax.swing.JButton();
        aboutBtn = new javax.swing.JButton();
        in_process_plugin = new javax.swing.JCheckBox();
        process_per_site = new javax.swing.JRadioButton();
        process_per_tab = new javax.swing.JRadioButton();
        single_process = new javax.swing.JRadioButton();
        max_window = new javax.swing.JCheckBox();
        label_others = new javax.swing.JLabel();
        language = new javax.swing.JComboBox<String>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle(this.get_trans("window_title"));

        label_sec.setForeground(new java.awt.Color(0, 102, 255));
        label_sec.setText(get_trans("security_setting"));

        file_access_file.setText(get_trans("disallow_read_local_file"));
        file_access_file.setToolTipText("--allow-file-access-from-files");

        disable_security.setText(get_trans("disable_security"));
        disable_security.setToolTipText("--disable-web-security");
        disable_security.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jyaqfhActionPerformed(evt);
            }
        });

        nosandbox.setText(get_trans("disable_sandbox"));
        nosandbox.setToolTipText("--no-sandbox");

        proc_settings.setForeground(new java.awt.Color(51, 102, 255));
        proc_settings.setText(get_trans("process_setting"));

        label_content.setForeground(new java.awt.Color(0, 102, 255));
        label_content.setText(get_trans("content_setting"));

        disable_js.setText(get_trans("disable_js"));
        disable_js.setToolTipText("--disable-javascript");

        disable_java.setText(get_trans("disable_java"));
        disable_java.setToolTipText("--disable-java");

        disable_plugins.setText(get_trans("disable_plugins"));
        disable_plugins.setToolTipText("--disable-plugins");

        disable_images.setText(get_trans("disable_images"));
        disable_images.setToolTipText("--disable-images");

        disk_cache.setText(get_trans("max_cache"));
        disk_cache.setToolTipText("--disk-cache-size=");

        hczdz1.setText("536870912");

        dmthczdz0.setText(get_trans("media_cache_max"));
        dmthczdz0.setToolTipText("--media-cache-size=");

        dmthczdz1.setText("1073741824");

        jLabel5.setText("byte");

        jLabel6.setText("byte");

        jLabel7.setForeground(new java.awt.Color(0, 102, 255));
        jLabel7.setText(get_trans("privacy"));

        jrysms.setText(get_trans("incognito"));
        jrysms.setToolTipText("--incognito");

        qyzhqhcd.setText(get_trans("enable_profiles"));
        qyzhqhcd.setToolTipText("--enable-udd-profiles");

        jLabel8.setForeground(new java.awt.Color(0, 102, 255));
        jLabel8.setText(get_trans("bookmark_settings"));

        qzqysqtb.setText(get_trans("force_bookmark_sync"));
        qzqysqtb.setToolTipText("--enable-sync");

        zgjlxssqan.setText(get_trans("show_bookmark_button"));
        zgjlxssqan.setToolTipText("--bookmark-menu");

        yxwytcck.setText(get_trans("allow_popup"));
        yxwytcck.setToolTipText("--disable-popup-blocking");

        szyy0.setText(get_trans("set_lang"));
        szyy0.setToolTipText("--lang=");

        szyy1.setEditable(true);
        szyy1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "zh-CN" }));

        pac0.setText(get_trans("pac_url"));
        pac0.setToolTipText("--proxy-pac-url  [via 1/2]");

        jLabel9.setForeground(new java.awt.Color(255, 0, 204));
        jLabel9.setText(get_trans("launcher_settings"));

        autorun.setText(get_trans("autolaunch"));

        startBtn.setText(get_trans("use_these_settings"));
        startBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startBtnActionPerformed(evt);
            }
        });

        cmdBtn.setText(get_trans("generate_command"));

        aboutBtn.setText(get_trans("help_and_update"));

        in_process_plugin.setForeground(new java.awt.Color(0, 102, 255));
        in_process_plugin.setText(get_trans("in_process_plugins"));
        in_process_plugin.setToolTipText("--in-process-plugins");

        process_per_site.setText(get_trans("process_per_site"));
        process_per_site.setToolTipText("--process-per-site");

        process_per_tab.setSelected(true);
        process_per_tab.setText(get_trans("process_per_tab"));
        process_per_tab.setToolTipText("--process-per-tab");

        single_process.setText(get_trans("single_process"));
        single_process.setToolTipText("--single-process");

        max_window.setText(get_trans("max_window"));
        max_window.setToolTipText("--start-maximized");

        label_others.setForeground(new java.awt.Color(0, 102, 255));
        label_others.setText(get_trans("label_others"));

        language.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "简体中文" }));

        panel = (JPanel) getContentPane();
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(panel);
        panel.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(pac0)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(pac1))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(language, 0, 119, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(aboutBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmdBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(startBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(single_process)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(process_per_tab)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(process_per_site)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(in_process_plugin))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(disk_cache)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(hczdz1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(dmthczdz0)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(dmthczdz1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel6))
                            .addComponent(label_sec)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(file_access_file)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(disable_security)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(nosandbox))
                            .addComponent(label_content)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(disable_js)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(disable_java)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(disable_plugins)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(disable_images)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(yxwytcck))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jrysms)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(qyzhqhcd))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(qzqysqtb)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(zgjlxssqan))
                            .addComponent(proc_settings)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(label_others)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(szyy0)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(szyy1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(max_window))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(autorun)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(label_sec)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(file_access_file)
                    .addComponent(disable_security)
                    .addComponent(nosandbox))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(proc_settings)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(single_process)
                    .addComponent(process_per_tab)
                    .addComponent(process_per_site)
                    .addComponent(in_process_plugin))
                .addGap(8, 8, 8)
                .addComponent(label_content)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(disable_js)
                    .addComponent(disable_java)
                    .addComponent(disable_plugins)
                    .addComponent(disable_images)
                    .addComponent(yxwytcck))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(disk_cache)
                    .addComponent(hczdz1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dmthczdz0)
                    .addComponent(jLabel5)
                    .addComponent(dmthczdz1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jrysms)
                    .addComponent(qyzhqhcd))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(qzqysqtb)
                    .addComponent(zgjlxssqan))
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(szyy0)
                    .addComponent(szyy1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(max_window)
                    .addComponent(label_others))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pac0)
                    .addComponent(pac1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(autorun))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(startBtn)
                    .addComponent(cmdBtn)
                    .addComponent(aboutBtn)
                    .addComponent(language, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }

    private class FindChromiumAction extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {

            JFileChooser fdia = new JFileChooser(chromium_exe);

            int ret = fdia.showDialog(panel, "Ok");

            if (ret == JFileChooser.APPROVE_OPTION) {
                File file = fdia.getSelectedFile();
                chromium_exe = file.getAbsolutePath();
            }
        }
    }

    private void jyaqfhActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jyaqfhActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jyaqfhActionPerformed

    private void startBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startBtnActionPerformed
        System.out.println("starting");
        if (startWindow.mcv == null) {
            startWindow.mcv = this;
        }
        startWindow.setVisible(true);
        startWindow.startCmd(creCmd());
        this.setVisible(false);
    }//GEN-LAST:event_startBtnActionPerformed

    private String creCmd() {
        String cmd = chromium_exe;
        if (file_access_file.isSelected()) {
            cmd = cmd + " " + file_access_file.getToolTipText();
        }
        if (disable_security.isSelected()) {
            cmd = cmd + " " + disable_security.getToolTipText();
        }
        if (nosandbox.isSelected()) {
            cmd = cmd + " " + nosandbox.getToolTipText();
        }
        if (proc_settings.isSelected()) {
            if (single_process.isSelected()) {
                cmd = cmd + " " + single_process.getToolTipText();
            }
            if (process_per_tab.isSelected()) {
                cmd = cmd + " " + process_per_tab.getToolTipText();
            }
            if (process_per_site.isSelected()) {
                cmd = cmd + " " + process_per_site.getToolTipText();
            }
        }
        if (in_process_plugin.isSelected()) {
                cmd = cmd + " " + in_process_plugin.getToolTipText();
        }
        if (disable_js.isSelected()) {
                cmd = cmd + " " + disable_js.getToolTipText();
        }
        if (disable_java.isSelected()) {
                cmd = cmd + " " + disable_java.getToolTipText();
        }
        if (disable_plugins.isSelected()) {
                cmd = cmd + " " + disable_plugins.getToolTipText();
        }
        if (disable_images.isSelected()) {
                cmd = cmd + " " + disable_images.getToolTipText();
        }
        if (yxwytcck.isSelected()) {
                cmd = cmd + " " + yxwytcck.getToolTipText();
        }
        if (disk_cache.isSelected()) {
                cmd = cmd + " " + disk_cache.getToolTipText() + hczdz1.getText();
        }
        if (dmthczdz0.isSelected()) {
                cmd = cmd + " " + dmthczdz0.getToolTipText() + dmthczdz1.getText();
        }
        if (jrysms.isSelected()) {
                cmd = cmd + " " + jrysms.getToolTipText();
        }
        if (qyzhqhcd.isSelected()) {
                cmd = cmd + " " + qyzhqhcd.getToolTipText();
        }
        if (qzqysqtb.isSelected()) {
                cmd = cmd + " " + qzqysqtb.getToolTipText();
        }
        if (zgjlxssqan.isSelected()) {
                cmd = cmd + " " + zgjlxssqan.getToolTipText();
        }
        if (szyy0.isSelected()) {
                cmd = cmd + " " + szyy0.getToolTipText() + szyy1.getSelectedItem().toString();
        }
        if (max_window.isSelected()) {
                cmd = cmd + " " + max_window.getToolTipText();
        }
        if (pac0.isSelected()) {
                cmd = cmd + " " + pac0.getToolTipText() + "\"" + pac1.getText() + "\"";
        }
        return cmd;
    }


    private javax.swing.JButton aboutBtn;
    private javax.swing.JCheckBox autorun;
    private javax.swing.JCheckBox in_process_plugin;
    private javax.swing.JButton cmdBtn;
    private javax.swing.JRadioButton single_process;
    private javax.swing.JCheckBox dmthczdz0;
    private javax.swing.JTextField dmthczdz1;
    private javax.swing.JCheckBox disk_cache;
    private javax.swing.JTextField hczdz1;
    private javax.swing.JLabel label_path;
    private javax.swing.JLabel label_others;
    private javax.swing.JLabel label_sec;
    private javax.swing.JLabel label_content;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JCheckBox proc_settings;
    private javax.swing.JCheckBox jrysms;
    private javax.swing.JCheckBox disable_security;
    private javax.swing.JCheckBox disable_plugins;
    private javax.swing.JCheckBox disable_java;
    private javax.swing.JCheckBox disable_js;
    private javax.swing.JCheckBox disable_images;
    private javax.swing.JCheckBox file_access_file;
    private javax.swing.JComboBox<String> language;
    private javax.swing.JRadioButton process_per_tab;
    private javax.swing.JRadioButton process_per_site;
    private javax.swing.JCheckBox pac0;
    private javax.swing.JTextField pac1;
    private javax.swing.JCheckBox nosandbox;
    private javax.swing.JCheckBox qyzhqhcd;
    private javax.swing.JCheckBox qzqysqtb;
    private javax.swing.JButton startBtn;
    private javax.swing.JCheckBox szyy0;
    private javax.swing.JComboBox<String> szyy1;
    private javax.swing.JCheckBox yxwytcck;
    private javax.swing.JCheckBox max_window;
    private javax.swing.JCheckBox zgjlxssqan;
}

class ChromiumProfileFilter implements FilenameFilter {
    public boolean accept(File dir, String name) {
        if (name.equals("Default")) {
            return true;
        }
        if (name.startsWith("Profile ")) {
            return true;
        }
        return false;
    }
}
