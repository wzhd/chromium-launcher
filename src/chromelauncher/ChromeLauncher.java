/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chromelauncher;

/**
 *
 * @author 雅詩
 */

import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

public class ChromeLauncher {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Map<String, String> env = System.getenv();
        String localestr = env.get("LANG");
        if (localestr == null) {
            localestr = "en_GB.UTF-8";
        }
        int dot = localestr.indexOf('.');
        if (dot > 0) {
                localestr = localestr.substring(0, dot);
        }
        String[] words = localestr.split("_");
        String language = words[0];
        String country = words[1];
        Locale currentLocale = new Locale(language, country);
        ResourceBundle messages;
        try {
            messages = ResourceBundle.getBundle("MessagesBundle", currentLocale);
        } catch (java.util.MissingResourceException e){
            messages = ResourceBundle.getBundle(
                                   "MessagesBundle",
                                   new Locale("en", "GB"));
        }
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("GTK+".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        System.out.println("=== yaChat ===");
        MainWindow mvc = new MainWindow(messages);
         mvc.setVisible(true);
    }
    
}
